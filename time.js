const STIME = {
     WEEKS :['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
 MONTHS : [null, 'Jan', 'Feb', 'Mar', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Nov', 'Dec'],

    toDate: function(obj) {
       if (obj instanceof Date)
           return obj;
       if (obj === undefined || obj === null)
           return new Date();
       return new Date(obj);
    },
    fromTimeOfDay: function(str) {
        var split = str.split(":");
        var hrs = split[0];
        split = split[1].split(" ");
        var isPM = split[1].trim() == "PM";
        if (isPM) {
            if (hrs != "12")
            hrs = parseInt(hrs) + 12;
        }else if (parseInt(hrs) === 12)
            hrs = 0;
        return [parseInt(hrs), parseInt(split[0].trim())];
    },
    getDate: function(date) {
        date = STIME.toDate(date);
        var day = date.getDay();
        var dayOM = date.getDate();
        var year = date.getFullYear();
        var month = date.getMonth();
        day = STIME.WEEKS[day];
        month = STIME.MONTHS[month];
        return day + ", " + month + " " + dayOM + " " + year;
    },

    getTimeOfDay: function(date) {
        date = STIME.toDate(date);

        var hour = date.getHours();
        var min = date.getMinutes();
        var amPm = null;

        if (hour > 12) {
            hour = hour - 12;
            amPm = "PM";
        }else {
            amPm = "AM";
            if (hour === 0)
                hour = 12;
        }

        if (min < 10)
            min = "0" + min;

        return hour + ":" + min + " " + amPm;
    },
    getTimeOfDayAndSeconds: function(date) {
        date = STIME.toDate(date);

        var hour = date.getHours();
        var min = date.getMinutes();
        var amPm = null;

        if (hour > 12) {
            hour = hour - 12;
            amPm = "PM";
        }else {
            amPm = "AM";
            if (hour === 0)
                hour = 12;
        }

        if (min < 10)
            min = "0" + min;

        var seconds = date.getSeconds();
        if (seconds < 10)
            seconds = "0" + seconds;

        return hour + ":" + min + ":"+ seconds +" " + amPm;
    },

    isInToday: function(inputDate)
{
    inputDate = STIME.toDate(inputDate);
    var today = new Date();
    if(today.setHours(0,0,0,0) == inputDate.setHours(0,0,0,0)){ return true; }
    else { return false; }
}

};