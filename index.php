<?php

function get_info() {
    if (file_exists('file'))
        return json_decode(file_get_contents('file'), true);
    else
        return [];
}

function save_info($file) {
    file_put_contents("file", json_encode($file, JSON_PRETTY_PRINT));
}

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case "punchin":
            $project = urldecode($_GET['project']);
            $file = get_info();

            $last = $file[count($file)-1];
            if ($last && $last[2] == null)
                die("ERROR: Already punched in");

            $file[] = [$project, time(), null];
            save_info($file);
            break;
        case "punchout":
            $file =get_info();

            $last = $file[count($file)-1];
            if ($last[2] != null)
                die("ERROR: Already punched out");
            $file[count($file)-1][2] = time();
            save_info($file);
            break;
        case "remove":
            $file = get_info();
            $list = explode(',', $_GET['punches']);
            foreach ($list as $i)
                $file[$i] = null;
            save_info(array_values(array_filter($file)));
    }
    die(file_get_contents('file'));
}else {
    die(file_get_contents('index.html'));
}