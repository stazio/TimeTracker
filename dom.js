const SDOM = {
    //--------------------
    //| UTILS |
    //--------------------

    many: function(elems, actions) {
        return SDOM.elems(elems, function(elem) {
            walk(actions, function(action) {
                SDOM[action](elem);
            });
        });
    },

    //--------------------
    //| FINDING ELEMENTS |
    //--------------------
    elem: function(supposed) {
        if (supposed instanceof Element || supposed instanceof Window)
            return supposed;
        return document.querySelector(supposed);
    },

    elems: function(elems, cb, numArgs) {
        var elemed = [];

        if (!Array.isArray(elems)) {
            var elem = SDOM.elem(elems);
            cb(elem);
            return elem;
        }

        walk(elems, function (elem) {
            elem = SDOM.elem(elem);
            elemed.push(elem);
            cb(elem);
        });
        return elemed;
    },


    //-----------
    //| EVENTS |
    //-----------
    listen: function(elems, name, action) {
        var names = name.split(" ");
        return SDOM.elems(elems, function(elem) {
            walk(names, function(name) {
                elem.addEventListener(name, action);
            });
        }, 2);
    },

    unlisten: function(elems, name, action) {
        var names = name.split(" ");
        return SDOM.elems(elems, function(elem) {
            walk(names, function(name) {
                elem.removeEventListener(name, action);
            });
        }, 2);
    },


    //-----------
    //| STYLING |
    //-----------

    style: function(elems, name, val) {
        return SDOM.elems(elems, function (elem) {
            elem.style.setProperty(name, val);
        }, 2);
    },

    unstyle: function(elems, name) {
        return SDOM.elems(elems, function(elem) {
            elem.style.removeProperty(name);
        }, 1);
    },

    show: function(elems) {
        return SDOM.elems(elems, function(elem) {
            elem.style.removeProperty('display');
        });
    },

    hide: function(elems) {
        return SDOM.elems(elems, function (elem) {
            elem.style.setProperty('display', 'none');
        });
    },

    //--------------
    //| ATTRIBUTES |
    //--------------
    attr: function(elems, name, val) {
        if (val === undefined || val === null)
            return SDOM.unattr(elems, name);
        return SDOM.elems(elems, function (elem) {
            elem.setAttribute(name, val);
        }, 2);
    },
    unattr: function(elems, name) {
        return SDOM.elem(elems, function(elem) {
            elem.removeProperty(name);
        }, 1);
    },

    append: function(parents, children) {
        SDOM.elems(parents, function(parent) {
           SDOM.elems(children, function(child) {
               parent.appendChild(child);
           });
        });
    },

    next: function(elem) {
         elem = SDOM.elem(elem);

         var next = false;
        return walk(elem.parentNode.child, function(item, i, arr, cancel) {
            if (next) {
                return cancel(item);
            }
            if (elem === item)
                next = true;

        });
    },

    inner: function(elems, inner, isHTML) {
        return SDOM.elems(elems, function(elem) {
            if (inner !== undefined) {
                if (isHTML )
                    elem.innerHTML = inner;
                else
                    elem.innerText = inner;
            }
        });
    }
        ,
    create: function(name, inner, isHTML) {
        var elem = document.createElement(name);
        if (inner !== undefined) {
            if (isHTML )
                elem.innerHTML = inner;
            else
                elem.innerText = inner;
        }
        return elem;
    },
    value: function(elem) {
        return SDOM.elem(elem).value;
    },
    removeChildren: function(elem) {
        elem = SDOM.elem(elem);
        while (elem.firstChild)
            elem.removeChild(elem.firstChild);
    },
    remove: function(elem) {
        SDOM.elem(elem).remove();
    }
};