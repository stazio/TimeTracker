function walk(arr, cb, start, end) {
    if (end === undefined) {
        end = arr.length;
    }else if (end < 0)
        end = arr.length + end;

    if (start === undefined)
        start = 0;

    var doCancel = false;
    var ress = undefined;
    function _doCancel(res) {
        doCancel  =true;
        ress = res;
    }

    for (var i =start; i < end; i++) {
        if (arr.hasOwnProperty(i))
            cb(arr[i], i, arr, _doCancel);
        if (doCancel)
            return ress;
    }
}

var SUTILS = {
    http: function (method, action, cb, data) {
        var http = new XMLHttpRequest();
        http.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                cb(http.responseText);
            }
        };
        http.open(method, action);
        http.send(data);
    }
};